# Transfer service application

This is a simple transfer service application for handling accounts and initiate transfers.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

* [Maven 3.6.0]
* [Java 8]

### Deployment

To have the application up & running locally, build the project with the tests and generate the documentation

```
mvn clean package javadoc:javadoc
```

And run the application locally

```
mvn spring-boot:run
```

Alternatively, the included Maven wrapper can also be used

```
$ ./mvnw spring-boot:run
```

## Design

The application is a REST application which operates with Account and TransferRequest objects. Accounts can be created and retrieved, but cannot be 
modified directly after creation.

The application accepts TransferRequest objects for each account indicating a transfer from the given account. The transfer cannot be done if the
initiating account does not have enough balance. Only positive amounts can be transferred. If an error occurs, the application provides a message 
with some details describing the cause.

The application creates some accounts on startup to have some examples filled in already.

### Endpoints

| Endpoint | Description |
| --- | --- |
| `/accounts` | List all accounts or create a new account |
| `/accounts/{id}` | Show a particular account |
| `/accounts/{id}/initiate` | Initiate a transfer from the account |
| `/error` | Fallback for error handling |
| `/actuator` | Some additional details |

_Note:_ To retrieve the available actuator endpoints use the management port, for example

```
$ curl http://localhost:8081/actuator
```

To open the HAL browser, use a browser and go to

```
http://localhost:8080
```

### Examples

###### Retrieve the accounts

```
$ curl http://localhost:8080/accounts
```

###### Retrieve a particular account

```
$ curl http://localhost:8080/accounts/1
```

###### Create a new account

```
$ curl -d '{"name": "ABC", "balance": 15}' -H "Content-Type: application/json;charset=UTF-8" -X POST http://localhost:8080/accounts
```

###### Initiate a transfer
```
$ curl -d '{"toAccount": 2, "amount": 0.1}' -H "Content-Type: application/json;charset=UTF-8" -X POST http://localhost:8080/accounts/1/initiate
```

### Limitations and possible future improvement possibilities

* The accounts cannot be deleted or modified after creation (by design).
* The same name can be used for two different accounts (the name is not unique by design).
* The transfers could be stored and retrieved to show history.
* The user could provide a transfer list from a particular account to initiate transfers after each other.
* Handling currencies.

## Technology choices

* [Spring Boot]
  - [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready)
  - [Spring Data REST](https://docs.spring.io/spring-data/rest/docs/current/reference/html/)
  - [Spring HATEOAS](https://projects.spring.io/spring-hateoas/)
  - [Spring Data JPA](https://projects.spring.io/spring-data-jpa/)
  - [Spring Boot Devtools](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-devtools.html)
* [H2]
* [JUnit]
* [JUnitParams]
* [Apache Commons Lang]
* [Maven]
* [Git]

## Built With

* [Maven 3.5.3](https://maven.apache.org) - Build & Dependency Management
* [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - JDK 1.8.0_171
