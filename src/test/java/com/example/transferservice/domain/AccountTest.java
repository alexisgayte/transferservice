package com.example.transferservice.domain;

import org.junit.Test;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {

    @Test
    public void testTransfer() {
        final Account fromAccount = new Account("William Johnson", TEN);
        final Account toAccount = new Account("Anna Roberts", valueOf(2));
        fromAccount.transfer(toAccount, ONE);
        assertThat(fromAccount.getBalance()).isEqualTo(valueOf(9));
        assertThat(toAccount.getBalance()).isEqualTo(valueOf(3));
    }
}