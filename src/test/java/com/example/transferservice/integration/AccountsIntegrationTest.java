package com.example.transferservice.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.transferservice.domain.Account;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

/**
 * Integration test for the application.
 * <p>
 * The test loads the application context and performs some basic operation through the REST endpoints.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestPropertySource(properties = {"management.port=0"})
public class AccountsIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetAccounts() {
        final ParameterizedTypeReference<Resources<Resource<Account>>> typeReference = new ParameterizedTypeReference<Resources<Resource<Account>>>
                () {
        };
        final ResponseEntity<Resources<Resource<Account>>> exchange = restTemplate.exchange("http://localhost:" + port + "/accounts", GET,
                HttpEntity.EMPTY, typeReference);
        final Collection<Resource<Account>> content = exchange.getBody().getContent();
        final List<Account> actualAccounts = content.stream().map(Resource::getContent).collect(Collectors.toList());

        final List<Account> expectedAccounts = createExpectedAccounts();
        assertThat(actualAccounts).isEqualTo(expectedAccounts);
    }

    @Test
    public void testGetAccount() {
        final ParameterizedTypeReference<Resource<Account>> typeReference = new ParameterizedTypeReference<Resource<Account>>() {
        };
        final ResponseEntity<Resource<Account>> exchange = restTemplate.exchange("http://localhost:" + port + "/accounts/" + 2, GET, HttpEntity
                .EMPTY, typeReference);
        final Account actualAccount = exchange.getBody().getContent();
        assertThat(actualAccount).isEqualTo(createExpectedAccounts().get(1));
    }

    @DirtiesContext
    @Test
    public void testPostAccounts() {
        final ParameterizedTypeReference<Resource<Account>> typeReference = new ParameterizedTypeReference<Resource<Account>>() {
        };
        final Account expected = new Account("Stewart Martin", valueOf(1.0));
        final HttpEntity<Account> accountHttpEntity = new HttpEntity<>(expected);
        final ResponseEntity<Resource<Account>> actual = restTemplate.exchange("http://localhost:" + port + "/accounts", POST, accountHttpEntity,
                typeReference);
        assertThat(actual.getBody().getContent()).isEqualTo(expected);
    }

    private List<Account> createExpectedAccounts() {
        return Arrays.asList(new Account("John Doe", valueOf(14.61)), new Account("William Smith", valueOf(2.78)), new Account("Jane Doe", valueOf
                (63.49)), new Account("Jimmy Brown", valueOf(46.82)));
    }
}