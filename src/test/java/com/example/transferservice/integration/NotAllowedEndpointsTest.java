package com.example.transferservice.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.EnumSet;

import static java.util.EnumSet.of;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.HEAD;
import static org.springframework.http.HttpMethod.OPTIONS;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.TRACE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class NotAllowedEndpointsTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testInitialize() throws Exception {
        helper(of(POST, OPTIONS, TRACE), "/accounts/1/initiate");
    }

    @Test
    public void testAccounts() throws Exception {
        mockMvc.perform(delete("/accounts")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void testAccount() throws Exception {
        helper(of(HEAD, GET, OPTIONS, TRACE), "/accounts/1");
    }

    private void helper(final EnumSet<HttpMethod> allowedMethods, final String urlTemplate) throws Exception {
        for (final HttpMethod httpMethod : HttpMethod.values()) {
            if (!allowedMethods.contains(httpMethod)) {
                mockMvc.perform(request(httpMethod, urlTemplate)).andDo(print()).andExpect(status().isMethodNotAllowed());
            }
        }
    }
}