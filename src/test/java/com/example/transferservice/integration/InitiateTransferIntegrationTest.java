package com.example.transferservice.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.transferservice.domain.Account;
import com.example.transferservice.domain.TransferRequest;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestPropertySource(properties = {"management.port=0"})
public class InitiateTransferIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @DirtiesContext
    @Test
    public void testTransfer() {
        final Account beforeTransferFromAccount = getAccount(1L);
        final Account beforeTransferToAccount = getAccount(2L);
        final BigDecimal amount = ONE;
        final ParameterizedTypeReference<Resource<Account>> typeReference = new ParameterizedTypeReference<Resource<Account>>() {
        };
        final TransferRequest expected = new TransferRequest(2L, amount);
        final HttpEntity<TransferRequest> accountHttpEntity = new HttpEntity<>(expected);
        final ResponseEntity<Resource<Account>> responseEntity = restTemplate.exchange("http://localhost:" + port + "/accounts/1/initiate", POST,
                accountHttpEntity, typeReference);

        final Account afterTransferFromAccount = responseEntity.getBody().getContent();
        final Account afterTransferToAccount = getAccount(2L);

        assertThat(afterTransferFromAccount.getBalance().add(amount)).isEqualTo(beforeTransferFromAccount.getBalance());
        assertThat(afterTransferToAccount.getBalance().subtract(amount)).isEqualTo(beforeTransferToAccount.getBalance());
    }

    private Account getAccount(final long n) {
        final ParameterizedTypeReference<Resource<Account>> typeReference = new ParameterizedTypeReference<Resource<Account>>() {
        };
        final ResponseEntity<Resource<Account>> exchange = restTemplate.exchange("http://localhost:" + port + "/accounts/" + n, GET, HttpEntity
                .EMPTY, typeReference);
        return exchange.getBody().getContent();
    }
}