package com.example.transferservice.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

import com.example.transferservice.domain.TransferRequest;
import com.example.transferservice.service.api.TransferService;
import com.example.transferservice.service.validation.TransferRequestValidator;

import static java.math.BigDecimal.ONE;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TransferControllerTest {
    @Mock
    private TransferService service;
    @Mock
    private TransferRequestValidator validator;
    private TransferController controller;

    @Before
    public void setUp() {
        controller = new TransferController(service, validator);
    }

    @Test
    public void testTransfer() {
        final BindingResult bindingResult = mock(BindingResult.class);
        final TransferRequest transferRequest = new TransferRequest(2L, ONE);
        controller.transfer(1L, transferRequest, bindingResult);
        verify(validator).validate(transferRequest, bindingResult);
        verify(service).transfer(1L, transferRequest);
    }
}