package com.example.transferservice.controller;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.example.transferservice.domain.error.TransferServiceError;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationErrorControllerTest {
    private ApplicationErrorController controller;

    @Before
    public void setUp() {
        controller = new ApplicationErrorController();
    }

    @Test
    public void testErrorPath() {
        assertThat(controller.getErrorPath()).isEqualTo("/error");
    }

    @Test
    public void testError() {
        final TransferServiceError expected = new TransferServiceError(HttpStatus.INTERNAL_SERVER_ERROR, "This endpoint is not supported.");
        final TransferServiceError actual = controller.errorOther();
        assertThat(actual.getStatus()).isEqualTo(expected.getStatus());
        assertThat(actual.getMessages()).isEqualTo(expected.getMessages());
    }
}