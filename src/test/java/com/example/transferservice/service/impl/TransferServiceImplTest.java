package com.example.transferservice.service.impl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.transferservice.domain.Account;
import com.example.transferservice.domain.TransferRequest;
import com.example.transferservice.domain.exception.TransferServiceException;
import com.example.transferservice.repository.AccountRepository;

import java.math.BigDecimal;
import java.util.Optional;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceImplTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    private TransferServiceImpl service;
    @Mock
    private AccountRepository repository;

    @Before
    public void setUp() {
        service = new TransferServiceImpl(repository);
    }

    @Test
    public void testNormal() {
        final Account fromAccount = mockAccount(1L, TEN);
        final Account toAccount = mockAccount(2L, valueOf(2));
        final TransferRequest transferRequest = new TransferRequest(2L, ONE);
        when(repository.findById(anyLong())).thenReturn(Optional.of(fromAccount), Optional.of(toAccount));
        service.transfer(1L, transferRequest);
        verify(repository, times(2)).findById(anyLong());
        verify(repository, times(2)).save(any());
        verify(fromAccount).transfer(toAccount, ONE);
        verify(toAccount, never()).transfer(any(), any());
    }

    @Test
    public void testFromAccountDoesNotExist() {
        final Account fromAccount = mockAccount(1L, TEN);
        final Account toAccount = mockAccount(2L, valueOf(2));
        final TransferRequest transferRequest = new TransferRequest(2L, ONE);
        when(repository.findById(anyLong())).thenReturn(Optional.empty(), Optional.of(toAccount));
        exception.expect(TransferServiceException.class);
        service.transfer(1L, transferRequest);
        verify(repository, times(2)).findById(anyLong());
        verify(repository, never()).save(any());
        verify(fromAccount, never()).transfer(any(), any());
        verify(toAccount, never()).transfer(any(), any());
    }

    @Test
    public void testToAccountDoesNotExist() {
        final Account fromAccount = mockAccount(1L, TEN);
        final Account toAccount = mockAccount(2L, valueOf(2));
        final TransferRequest transferRequest = new TransferRequest(2L, ONE);
        when(repository.findById(anyLong())).thenReturn(Optional.of(fromAccount), Optional.empty());
        exception.expect(TransferServiceException.class);
        service.transfer(1L, transferRequest);
        verify(repository, times(2)).findById(anyLong());
        verify(repository, never()).save(any());
        verify(fromAccount, never()).transfer(any(), any());
        verify(toAccount, never()).transfer(any(), any());
    }

    @Test
    public void testSameAccounts() {
        final Account fromAccount = new Account("William Johnson", TEN);
        final Account toAccount = new Account("Anna Roberts", valueOf(2));
        final TransferRequest transferRequest = new TransferRequest(2L, ONE);
        when(repository.findById(anyLong())).thenReturn(Optional.of(fromAccount), Optional.of(toAccount));
        exception.expect(TransferServiceException.class);
        service.transfer(1L, transferRequest);
        verify(repository, times(2)).findById(anyLong());
        verify(repository, never()).save(any());
        verify(fromAccount, never()).transfer(any(), any());
        verify(toAccount, never()).transfer(any(), any());
    }

    @Test
    public void testNotEnoughBalance() {
        final Account fromAccount = mockAccount(1L, valueOf(9));
        final Account toAccount = mockAccount(2L, valueOf(2));
        final TransferRequest transferRequest = new TransferRequest(2L, TEN);
        when(repository.findById(anyLong())).thenReturn(Optional.of(fromAccount), Optional.of(toAccount));
        exception.expect(TransferServiceException.class);
        service.transfer(1L, transferRequest);
        verify(repository, times(2)).findById(anyLong());
        verify(repository, never()).save(any());
        verify(fromAccount, never()).transfer(any(), any());
        verify(toAccount, never()).transfer(any(), any());
    }

    private Account mockAccount(final long accountId, final BigDecimal balance) {
        final Account mock = mock(Account.class);
        when(mock.getAccountId()).thenReturn(accountId);
        when(mock.getBalance()).thenReturn(balance);
        return mock;
    }
}