package com.example.transferservice.service.validation;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.example.transferservice.domain.TransferRequest;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferRequestValidatorTest {
    @Test
    public void testNormal() {
        final TransferRequestValidator validator = new TransferRequestValidator();
        final TransferRequest transferRequest = new TransferRequest(2, BigDecimal.ONE);
        final Errors errors = new BeanPropertyBindingResult(transferRequest, "transferRequest");
        validator.validate(transferRequest, errors);
        assertThat(errors.hasErrors()).isFalse();
    }

    @Test
    public void testEmptyAmount() {
        final TransferRequestValidator validator = new TransferRequestValidator();
        final TransferRequest transferRequest = new TransferRequest(2, null);
        final Errors errors = new BeanPropertyBindingResult(transferRequest, "transferRequest");
        validator.validate(transferRequest, errors);
        assertThat(errors.hasErrors()).isTrue();
        final FieldError amount = errors.getFieldError("amount");
        assertThat(amount.getDefaultMessage()).isEqualTo("The property must not be empty.");
    }

    @Test
    public void testNegativeAmount() {
        final TransferRequestValidator validator = new TransferRequestValidator();
        final TransferRequest transferRequest = new TransferRequest(2, BigDecimal.valueOf(-2));
        final Errors errors = new BeanPropertyBindingResult(transferRequest, "transferRequest");
        validator.validate(transferRequest, errors);
        assertThat(errors.hasErrors()).isTrue();
        final FieldError amount = errors.getFieldError("amount");
        assertThat(amount.getDefaultMessage()).isEqualTo("The property must be positive.");
    }

    @Test
    public void testNegatives() {
        final TransferRequestValidator validator = new TransferRequestValidator();
        final TransferRequest transferRequest = new TransferRequest(-7, BigDecimal.valueOf(-2));
        final Errors errors = new BeanPropertyBindingResult(transferRequest, "transferRequest");
        validator.validate(transferRequest, errors);
        assertThat(errors.hasErrors()).isTrue();
        final FieldError amount = errors.getFieldError("amount");
        assertThat(amount.getDefaultMessage()).isEqualTo("The property must be positive.");
        final FieldError toAccount = errors.getFieldError("toAccount");
        assertThat(toAccount.getDefaultMessage()).isEqualTo("The property must be positive.");
    }
}