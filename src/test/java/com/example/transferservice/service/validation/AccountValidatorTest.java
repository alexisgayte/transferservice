package com.example.transferservice.service.validation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import junitparams.JUnitParamsRunner;
import junitparams.NamedParameters;
import junitparams.Parameters;

import com.example.transferservice.domain.Account;

import java.util.Optional;

import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class AccountValidatorTest {
    @Test
    @Parameters(named = "return accounts")
    public void testEmpty(final Account account, final String fieldName, final Optional<String> errorMessage) {
        final AccountValidator validator = new AccountValidator();
        final Errors errors = new BeanPropertyBindingResult(account, "account");
        validator.validate(account, errors);
        if (errorMessage.isPresent()) {
            assertThat(errors.hasErrors()).isTrue();
            assertThat(errors.getFieldErrorCount(fieldName)).isEqualTo(1);
            final FieldError fieldError = errors.getFieldError(fieldName);
            assertThat(fieldError.getDefaultMessage()).isEqualTo("The property must not be empty.");
        } else {
            assertThat(errors.hasErrors()).isFalse();
        }
    }

    @NamedParameters("return accounts")
    private Object[] returnEmptyFieldAccounts() {
        return new Object[]{
                new Object[]{new Account("William Johnson", TEN), "name", empty()},
                new Object[]{new Account("", TEN), "name", of("The property must not be empty.")},
                new Object[]{new Account("  ", TEN), "name", of("The property must not be empty.")},
                new Object[]{new Account(null, TEN), "name", of("The property must not be empty.")},
                new Object[]{new Account("William Johnson", null), "balance", of("The property must not be empty.")}
        };
    }

    @Test
    public void testNegativeBalance() {
        final AccountValidator validator = new AccountValidator();
        final Account account = new Account("William Johnson", valueOf(-1));
        final Errors errors = new BeanPropertyBindingResult(account, "account");
        validator.validate(account, errors);
        assertThat(errors.hasErrors()).isTrue();
        assertThat(errors.getFieldErrorCount("balance")).isEqualTo(1);
        final FieldError amount = errors.getFieldError("balance");
        assertThat(amount.getDefaultMessage()).isEqualTo("The property must not be negative.");
    }
}