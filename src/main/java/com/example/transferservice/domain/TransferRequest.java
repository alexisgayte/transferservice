package com.example.transferservice.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public final class TransferRequest {
    private final long toAccount;
    private final BigDecimal amount;

    @JsonCreator
    public TransferRequest(@JsonProperty("toAccount") final long toAccount, @JsonProperty("amount") final BigDecimal amount) {
        this.toAccount = toAccount;
        this.amount = amount;
    }

    public long getToAccount() {
        return toAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}