package com.example.transferservice.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public final class Account extends ResourceSupport {

    @Id
    @GeneratedValue
    private long accountId;
    private String name;
    private BigDecimal balance;

    // JPA only
    private Account() {
    }

    @JsonCreator
    public Account(@JsonProperty("name") final String name, @JsonProperty("balance") final BigDecimal balance) {
        this.name = name;
        this.balance = balance;
    }

    public void transfer(final Account account, final BigDecimal amount) {
        balance = balance.subtract(amount);
        account.balance = account.balance.add(amount);
    }

    public long getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Account account = (Account) o;

        return new EqualsBuilder().appendSuper(super.equals(o)).append(accountId, account.accountId).append(name, account.name).append(balance,
                account.balance).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).appendSuper(super.hashCode()).append(accountId).append(name).append(balance).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("accountId", accountId).append("name", name).append("balance", balance).toString();
    }
}