package com.example.transferservice.domain.error;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class TransferServiceError {
    private final HttpStatus status;
    private final List<String> messages;

    public TransferServiceError(final HttpStatus status, final Collection<String> messages) {
        this.status = status;
        this.messages = new ArrayList<>(messages);
    }

    public TransferServiceError(final HttpStatus status, final String message) {
        this(status, Arrays.asList(message));
    }

    public HttpStatus getStatus() {
        return status;
    }

    /**
     * Returns an immutable list of the error messages.
     *
     * @return the error messages
     */
    public List<String> getMessages() {
        return Collections.unmodifiableList(messages);
    }
}