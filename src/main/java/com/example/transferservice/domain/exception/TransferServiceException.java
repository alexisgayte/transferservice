package com.example.transferservice.domain.exception;

public class TransferServiceException extends RuntimeException {
    private static final long serialVersionUID = 42L;

    public TransferServiceException(final String message) {
        super(message);
    }
}