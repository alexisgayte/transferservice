package com.example.transferservice.controller;

import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PutMapping;

@BasePathAwareController
public class AccountController {

    @PutMapping(value = "/accounts/{id}")
    public ResponseEntity<?> preventsPut(final HttpMethod httpMethod) throws HttpRequestMethodNotSupportedException {
        throw new HttpRequestMethodNotSupportedException(httpMethod.toString());
    }

    @PatchMapping(value = "/accounts/{id}")
    public ResponseEntity<?> preventsPatch(final HttpMethod httpMethod) throws HttpRequestMethodNotSupportedException {
        throw new HttpRequestMethodNotSupportedException(httpMethod.toString());
    }
}