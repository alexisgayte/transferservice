package com.example.transferservice.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.transferservice.domain.error.TransferServiceError;

@RestController
public class ApplicationErrorController implements ErrorController {

    private static final String PATH = "/error";

    @Override
    public String getErrorPath() {
        return PATH;
    }

    /**
     * Returns the error response.
     *
     * @return the error
     */
    @RequestMapping(value = PATH)
    TransferServiceError errorOther() {
        return new TransferServiceError(HttpStatus.INTERNAL_SERVER_ERROR, "This endpoint is not supported.");
    }
}