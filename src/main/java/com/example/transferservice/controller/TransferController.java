package com.example.transferservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.transferservice.domain.TransferRequest;
import com.example.transferservice.service.api.TransferService;
import com.example.transferservice.service.validation.TransferRequestValidator;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class TransferController {

    private final TransferService transferService;
    private final TransferRequestValidator validator;

    @Autowired
    public TransferController(final TransferService transferService, final TransferRequestValidator validator) {
        this.transferService = transferService;
        this.validator = validator;
    }

    @PostMapping(path = "/accounts/{id}/initiate", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<?> transfer(@PathVariable long id, @RequestBody final TransferRequest transferRequest, BindingResult result) {
        validator.validate(transferRequest, result);
        if (result.hasErrors()) {
            throw new RepositoryConstraintViolationException(result);
        }
        return ResponseEntity.ok(transferService.transfer(id, transferRequest));
    }
}