package com.example.transferservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.transferservice.domain.Account;
import com.example.transferservice.repository.AccountRepository;

import static java.math.BigDecimal.valueOf;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner initAccounts(final AccountRepository accountRepository) {
        return (args) -> {
            accountRepository.save(new Account("John Doe", valueOf(14.61)));
            accountRepository.save(new Account("William Smith", valueOf(2.78)));
            accountRepository.save(new Account("Jane Doe", valueOf(63.49)));
            accountRepository.save(new Account("Jimmy Brown", valueOf(46.82)));
        };
    }
}