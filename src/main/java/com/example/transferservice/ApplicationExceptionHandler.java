package com.example.transferservice;

import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.transferservice.domain.error.TransferServiceError;
import com.example.transferservice.domain.exception.TransferServiceException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException e, final HttpHeaders headers, final
    HttpStatus status, final WebRequest request) {
        final TransferServiceError transferServiceError = new TransferServiceError(HttpStatus.BAD_REQUEST, Collections.singletonList(e
                .getLocalizedMessage()));
        return new ResponseEntity<>(transferServiceError, new HttpHeaders(), transferServiceError.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(final Exception e, final Object body, final HttpHeaders headers, final HttpStatus
            status, final WebRequest request) {
        final TransferServiceError transferServiceError = new TransferServiceError(status, Collections.singletonList(e.getLocalizedMessage()));
        return new ResponseEntity<>(transferServiceError, new HttpHeaders(), transferServiceError.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(final HttpRequestMethodNotSupportedException e, final HttpHeaders headers,
                                                                         final HttpStatus status, final WebRequest request) {
        final TransferServiceError transferServiceError = new TransferServiceError(status, Collections.singletonList(e.getLocalizedMessage()));
        return new ResponseEntity<>(transferServiceError, headers, transferServiceError.getStatus());
    }

    /**
     * Customize the response for {@link RepositoryConstraintViolationException}.
     *
     * @param e the exception
     *
     * @return a {@code TransferServiceError} instance
     */
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(RepositoryConstraintViolationException.class)
    TransferServiceError handleRepositoryConstraintViolationException(final RepositoryConstraintViolationException e) {
        final List<String> errors = e.getErrors().getAllErrors().stream().map(ObjectError::toString).collect(Collectors.toList());
        return new TransferServiceError(HttpStatus.NOT_ACCEPTABLE, errors);
    }

    /**
     * Customize the response for {@link TransferServiceException}.
     *
     * @param e the exception
     *
     * @return a {@code TransferServiceError} instance
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(TransferServiceException.class)
    TransferServiceError handleTransferServiceException(final TransferServiceException e) {
        return new TransferServiceError(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
    }

    /**
     * Customize the response for all the unhandled exceptions.
     *
     * @param e the exception
     *
     * @return a {@code TransferServiceError} instance
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    TransferServiceError handleOtherExceptions(final Exception e) {
        return new TransferServiceError(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
    }
}