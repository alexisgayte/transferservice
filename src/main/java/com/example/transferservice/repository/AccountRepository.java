package com.example.transferservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.example.transferservice.domain.Account;

/**
 * Repository for the accounts.
 * <p>
 * Note: the DELETE operation is not exported.
 */
@RepositoryRestResource
public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {
    @Override
    @RestResource(exported = false)
    void deleteById(Long id);

    @Override
    @RestResource(exported = false)
    void delete(Account entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Account> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}