package com.example.transferservice.service.api;

import com.example.transferservice.domain.Account;
import com.example.transferservice.domain.TransferRequest;

public interface TransferService {
    /**
     * Initiates a transfer based on the request from the account with the given id.
     *
     * @param id              the id of the account where the transfer is initiated
     * @param transferRequest the request of the transfer
     *
     * @return the initiating account
     */
    Account transfer(long id, TransferRequest transferRequest);
}