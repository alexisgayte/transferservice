package com.example.transferservice.service.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.example.transferservice.domain.TransferRequest;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace;

/**
 * Represents a validator for the {@code TransferRequest} object.
 *
 * @see TransferRequest
 */
@Component
public class TransferRequestValidator implements Validator {

    private static final String EMPTY = "empty";
    private static final String NEGATIVEVALUE = "negativevalue";
    private static final String TO_ACCOUNT = "toAccount";
    private static final String AMOUNT = "amount";
    private static final String THE_PROPERTY_MUST_NOT_BE_EMPTY = "The property must not be empty.";
    private static final String THE_PROPERTY_MUST_BE_POSITIVE = "The property must be positive.";

    @Override
    public boolean supports(final Class<?> clazz) {
        return TransferRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        rejectIfEmptyOrWhitespace(errors, TO_ACCOUNT, EMPTY, THE_PROPERTY_MUST_NOT_BE_EMPTY);
        rejectIfEmptyOrWhitespace(errors, AMOUNT, EMPTY, THE_PROPERTY_MUST_NOT_BE_EMPTY);
        final TransferRequest transferRequest = (TransferRequest) target;
        if (transferRequest.getToAccount() <= 0L) {
            errors.rejectValue(TO_ACCOUNT, NEGATIVEVALUE, THE_PROPERTY_MUST_BE_POSITIVE);
        }
        final BigDecimal amount = transferRequest.getAmount();
        if (amount != null && amount.compareTo(ZERO) <= 0) {
            errors.rejectValue(AMOUNT, NEGATIVEVALUE, THE_PROPERTY_MUST_BE_POSITIVE);
        }
    }
}