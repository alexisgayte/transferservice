package com.example.transferservice.service.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.example.transferservice.domain.Account;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace;

/**
 * Represents a validator for the {@code Account} object.
 *
 */
@Component("beforeCreateAccountValidator")
public class AccountValidator implements Validator {

    private static final String NEGATIVEVALUE = "negativevalue";
    private static final String EMPTY = "empty";
    private static final String THE_PROPERTY_MUST_NOT_BE_EMPTY = "The property must not be empty.";
    private static final String NAME = "name";
    private static final String BALANCE = "balance";
    private static final String THE_PROPERTY_MUST_NOT_BE_NEGATIVE = "The property must not be negative.";

    @Override
    public boolean supports(final Class<?> clazz) {
        return Account.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        rejectIfEmptyOrWhitespace(errors, NAME, EMPTY, THE_PROPERTY_MUST_NOT_BE_EMPTY);
        rejectIfEmptyOrWhitespace(errors, BALANCE, EMPTY, THE_PROPERTY_MUST_NOT_BE_EMPTY);
        final Account account = (Account) target;
        final BigDecimal balance = account.getBalance();
        if (balance != null && balance.compareTo(ZERO) < 0) {
            errors.rejectValue(BALANCE, NEGATIVEVALUE, THE_PROPERTY_MUST_NOT_BE_NEGATIVE);
        }
    }
}