package com.example.transferservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.transferservice.domain.Account;
import com.example.transferservice.domain.TransferRequest;
import com.example.transferservice.domain.exception.TransferServiceException;
import com.example.transferservice.repository.AccountRepository;
import com.example.transferservice.service.api.TransferService;

import java.util.Optional;

/**
 * Implementation of the transfer service.
 * <p>
 * This implementation executes basic checks before actually executing the transfer.
 *
 */
@Service
public class TransferServiceImpl implements TransferService {
    private final AccountRepository accountRepository;

    @Autowired
    public TransferServiceImpl(final AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account transfer(final long id, final TransferRequest transferRequest) {
        final Optional<Account> fromAccountOptional = accountRepository.findById(id);
        final Optional<Account> toAccountOptional = accountRepository.findById(transferRequest.getToAccount());
        final Account fromAccount = fromAccountOptional.orElseThrow(() -> new TransferServiceException("The source account does not exist"));
        final Account toAccount = toAccountOptional.orElseThrow(() -> new TransferServiceException("The destination account does not exist"));
        validate(fromAccount, toAccount, transferRequest);
        fromAccount.transfer(toAccount, transferRequest.getAmount());
        accountRepository.save(fromAccount);
        accountRepository.save(toAccount);
        return fromAccount;
    }

    private void validate(final Account fromAccount, final Account toAccount, final TransferRequest transferRequest) {
        if (fromAccount.getBalance().compareTo(transferRequest.getAmount()) < 0) {
            throw new TransferServiceException("The source balance is smaller than the transfer amount");
        }
        if (fromAccount.getAccountId() == toAccount.getAccountId()) {
            throw new TransferServiceException("The source and the destination account is the same");
        }
    }
}